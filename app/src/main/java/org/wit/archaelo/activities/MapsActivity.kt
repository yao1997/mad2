package org.wit.archaelo.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_placemark.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.wit.archaelo.R
import org.wit.archaelo.models.LocationModel

class MapsActivity : AppCompatActivity(), OnMapReadyCallback,GoogleMap.OnMarkerDragListener,GoogleMap.OnMarkerClickListener,AnkoLogger {

  private lateinit var mMap: GoogleMap
  var lat=52.2459
  var lng=-7.1388
  var zoom=17.0f
  var location:LocationModel=LocationModel(52.2459,-7.1388,17.0f)

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_maps)
    setSupportActionBar(addPlacemark_toolbar)
    supportActionBar?.apply {
      setDisplayHomeAsUpEnabled(true)
      setHomeAsUpIndicator(R.drawable.toolbar_back)
      setDisplayShowTitleEnabled(false)
    }

    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    val mapFragment = supportFragmentManager
        .findFragmentById(R.id.map) as SupportMapFragment
    mapFragment.getMapAsync(this)
  }

  override fun onMapReady(googleMap: GoogleMap) {
    mMap = googleMap
    mMap.setOnMarkerDragListener(this)
    mMap.setOnMarkerClickListener(this)
    //location=LocationModel(52.2459,-7.1388,17.0f)
    if (intent.hasExtra("location")){
      location=intent.extras.getParcelable<LocationModel>("location")
    }
    // Add a marker in Sydney and move the camera
    val locationLatLng = LatLng(location.lat, location.lng)

    mMap.addMarker(MarkerOptions().position(locationLatLng).
        title("Current Location")
        .snippet("GPS : " + location.toString())
        .draggable(true))
    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationLatLng,zoom))
  }

  override fun onMarkerDragEnd(marker: Marker) {
    info("dragged")
    location.lat = marker.position.latitude
    location.lng = marker.position.longitude
    location.zoom = mMap.cameraPosition.zoom
    marker.setSnippet("GPS : " + location.toString())
    marker.showInfoWindow()
  }

  override fun onMarkerDragStart(marker: Marker) {
    //info("being dragged")
  }

  override fun onMarkerDrag(marker: Marker) {
    //info("being dragged")
  }

  override fun onMarkerClick(marker: Marker): Boolean {
    info("clicked")
    marker.showInfoWindow()
    marker.setSnippet("GPS : " + location.toString())
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      android.R.id.home->{
        onBackPressed()
        true
      }
      else->super.onOptionsItemSelected(item)
    }
  }

  override fun onBackPressed() {
    val resultIntent = Intent()
    resultIntent.putExtra("location", location)
    setResult(Activity.RESULT_OK, resultIntent)
    finish()
    super.onBackPressed()
  }
}
