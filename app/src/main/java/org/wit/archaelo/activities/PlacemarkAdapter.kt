package org.wit.archaelo.activities

import android.content.Context
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_placemark.view.*
import org.wit.archaelo.models.PlacemarkModel
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.wit.archaelo.R
import org.wit.archaelo.helpers.readImageFromPath
import java.io.InputStream
import java.net.URL


class PlacemarkAdapter(var placemarks:List<PlacemarkModel>,listener: PlacemarkListener): RecyclerView.Adapter<PlacemarkViewHolder>(){
  private val listener=listener
  override fun getItemCount()=placemarks.size

  override fun onBindViewHolder(holder: PlacemarkViewHolder, position: Int) {
    val placemark=placemarks[position]
    holder.bind(placemark,listener)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlacemarkViewHolder {
    return PlacemarkViewHolder(LayoutInflater.from(parent.context).inflate(org.wit.archaelo.R.layout.card_placemark,parent,false),parent.context)
  }

  fun updateData(newPlacemarks:List<PlacemarkModel>){
    this.placemarks=newPlacemarks
    this.notifyDataSetChanged()
  }
}

class PlacemarkViewHolder(itemView: View,context:Context): RecyclerView.ViewHolder(itemView),AnkoLogger{
  val context=context
  fun bind(placemark:PlacemarkModel,listener: PlacemarkListener){
    /*if (placemark.visited){
      itemView.supporting_text.text="Visited"
      itemView.supporting_text.setTextColor(ContextCompat.getColor(context,R.color.successColor))
    }
    else{
      itemView.supporting_text.text="Not Visited"
      itemView.supporting_text.setTextColor(ContextCompat.getColor(context,R.color.grayBackground))
    }*/
    //var content=URL(placemark.dp).content
    //var bitmap=BitmapFactory.decodeStream(content as InputStream)
    itemView.visited.text=if (placemark.visited) "Visited" else "Not Visited"
    itemView.card_name.text = placemark.name
    itemView.card_description.text = placemark.description
    itemView.ratingBar.rating=placemark.rating
    //itemView.media_image.setImageBitmap(bitmap)
    Picasso.get().load(placemark.dp).fit().centerCrop().placeholder(R.drawable.placeholder).
        into(itemView.media_image)
    itemView.setOnClickListener{
      listener.onPlacemarkClicked(placemark)
    }
  }
}

interface PlacemarkListener{
  fun onPlacemarkClicked(placemark: PlacemarkModel)
}