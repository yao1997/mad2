package org.wit.archaelo.activities

import org.jetbrains.anko.startActivityForResult
import org.wit.archaelo.main.MainApp
import org.wit.archaelo.models.PlacemarkModel
import org.wit.archaelo.helpers.alert
import org.wit.archaelo.R
import org.wit.archaelo.activities.placemark.PlacemarkView

class PlacemarkPresenter(val activity: PlacemarkView){
  private val ADD_PLACEMARK_RESULT_CODE=1
  private val REMOVE_PLACEMARK_RESULT_CODE=3
  val SELECT_IMAGE_REQUEST=1
  val SELECT_LOCATION_REQUEST=2
  val SELECT_DP_REQUEST=3


  var placemark=PlacemarkModel()
  var edit=false
  var app=activity.application as MainApp

  init{
    if (activity.intent.hasExtra("placemark")){
      edit=true
      placemark=activity.intent.getParcelableExtra("placemark")
      activity.initiateEditPlacemark(placemark)
    }
  }

  fun doAddOrEdit(name:String,description:String){
    placemark.name=name
    placemark.description=description
    if (edit){
      app.fireStore.update(placemark)
    }
    else{
      app.fireStore.create(placemark)
    }
    activity.setResult(ADD_PLACEMARK_RESULT_CODE)
    activity.finish()
  }

  fun removePlacemark(){
    app.fireStore.delete(placemark)
    activity.setResult(REMOVE_PLACEMARK_RESULT_CODE)
    activity.finish()
  }

  fun doSetLocation(){
    activity.startActivityForResult<MapsActivity>(SELECT_LOCATION_REQUEST, "location" to placemark.location)
  }

  fun validateInput(name:String,description: String,dp:String,images:MutableList<String>,visited:Boolean,visitedDate:String):Boolean{
    var result=false
    if (name.isBlank()){
      alert(activity,"Please fill in all fields","Placemark name is empty", R.color.errorColor)
    }
    else if (description.isBlank()){
      alert(activity,"Please fill in all fields","Placemark additional notes is empty", R.color.errorColor)
    }
    else if (dp.isBlank()){
      alert(activity,"Please upload image","Your lecturer requested a profile image as proof", R.color.errorColor)
    }
    /*else if (images.isEmpty()){
      alert(activity,"Please upload image","Your lecturer requested at least 1 image as proof", R.color.errorColor)
    }*/
    else if (visited && visitedDate==activity.resources.getText(R.string.set_placemark_date_hint)){
      alert(activity,"Please select visited date","Select a visited date or mark as unvisited.", R.color.errorColor)
    }
    else{
      result=true
    }
    return result
  }
}