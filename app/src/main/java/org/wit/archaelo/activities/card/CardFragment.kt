package org.wit.archaelo.activities.card

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import androidx.fragment.app.Fragment
import com.yuyakaido.android.cardstackview.*
import kotlinx.android.synthetic.main.fragment_card.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.wit.archaelo.R
import org.wit.archaelo.main.MainApp


class CardFragment: Fragment(), CardStackListener ,AnkoLogger{
  lateinit var app: MainApp
  lateinit var presenter: CardPresenter
  lateinit var manager: CardStackLayoutManager

  lateinit var adapter:CardStackAdapter

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    val view=inflater.inflate(R.layout.fragment_card,container,false)
    app= MainApp.applicationObject!!
    presenter= CardPresenter(this)
    return view
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    presenter.getPlacemark(fun(){
      manager=CardStackLayoutManager(this.activity,this)
      adapter= CardStackAdapter(presenter.placemarks)

      manager.setStackFrom(StackFrom.Right)
      cardStackView.layoutManager=manager
      cardStackView.adapter=adapter
      card_empty_text.visibility=if (presenter.placemarks.isEmpty())View.VISIBLE else View.GONE
      content.visibility=if (presenter.placemarks.isEmpty())View.GONE else View.VISIBLE
    })

    rewindBtn.setOnClickListener {
      cardStackView.rewind()
    }

    swipeLeftBtn.setOnClickListener {
      swipeLeft()
    }

    swipeRightBtn.setOnClickListener {
      swipeRight()
    }
    super.onViewCreated(view, savedInstanceState)
  }
  override fun onCardAppeared(view: View?, position: Int) {
  }

  override fun onCardCanceled() {
  }

  override fun onCardDisappeared(view: View?, position: Int) {
  }

  override fun onCardDragging(direction: Direction?, ratio: Float) {
  }

  override fun onCardRewound() {
  }

  override fun onCardSwiped(direction: Direction?) {
    if(direction==Direction.Right){
      presenter.changeFavourite(presenter.placemarks.get(manager.topPosition-1),true)
    }
  }

  fun swipeLeft(){
    val setting = SwipeAnimationSetting.Builder()
        .setDirection(Direction.Left)
        .setDuration(600)
        .setInterpolator(AccelerateInterpolator())
        .build()
    manager.setSwipeAnimationSetting(setting)
    cardStackView.swipe()
  }

  fun swipeRight(){
    val setting = SwipeAnimationSetting.Builder()
        .setDirection(Direction.Right)
        .setDuration(600)
        .setInterpolator(AccelerateInterpolator())
        .build()
    manager.setSwipeAnimationSetting(setting)
    cardStackView.swipe()
  }

  fun hideProgress(){
    card_progressBar.visibility=View.GONE
  }

  fun showProgress(){
    card_progressBar.visibility=View.VISIBLE
  }
}