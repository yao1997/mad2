package org.wit.archaelo.activities.card

import org.wit.archaelo.models.PlacemarkModel

class CardPresenter(val view:CardFragment){
  var app=view.app
  var placemarks= emptyList<PlacemarkModel>()


  fun getPlacemark(callback: () -> Unit=fun(){}){
    view.showProgress()
    app.fireStore.fetchPlacemarks {
      view.hideProgress()
      placemarks=app.fireStore.placemarks
      callback()
    }
  }

  fun changeFavourite(placemark:PlacemarkModel,value:Boolean){
    placemark.favourite=value
    app.fireStore.update(placemark)
  }
}