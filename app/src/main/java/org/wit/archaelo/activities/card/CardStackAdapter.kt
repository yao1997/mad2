package org.wit.archaelo.activities.card

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import org.wit.archaelo.R
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import org.wit.archaelo.models.PlacemarkModel

class CardStackAdapter(
    private var placemarks: List<PlacemarkModel> = emptyList()
) : RecyclerView.Adapter<CardStackAdapter.ViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val inflater = LayoutInflater.from(parent.context)
    return ViewHolder(inflater.inflate(R.layout.card_swipe, parent, false))
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val placemark = placemarks[position]
    holder.name.text = placemark.name
    holder.city.text = placemark.description
    Glide.with(holder.image)
        .load(placemark.dp)
        .into(holder.image)
    holder.itemView.setOnClickListener { v ->
      Toast.makeText(v.context, placemark.name, Toast.LENGTH_SHORT).show()
    }
  }

  override fun getItemCount(): Int {
    return placemarks.size
  }

  fun setPlacemarks(spots: List<PlacemarkModel>) {
    this.placemarks = spots
  }

  fun getPlacemarks(): List<PlacemarkModel> {
    return placemarks
  }

  class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val name: TextView = view.findViewById(R.id.item_name)
    var city: TextView = view.findViewById(R.id.item_city)
    var image: ImageView = view.findViewById(R.id.item_image)
  }

}