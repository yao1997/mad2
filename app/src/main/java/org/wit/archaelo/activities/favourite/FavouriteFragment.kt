package org.wit.archaelo.activities.favourite

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_favourite.*
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.AnkoLogger
import org.wit.archaelo.R
import org.wit.archaelo.activities.PlacemarkAdapter
import org.wit.archaelo.activities.PlacemarkListener
import org.wit.archaelo.activities.placemark.PlacemarkView
import org.wit.archaelo.main.MainApp
import org.wit.archaelo.models.PlacemarkModel

class FavouriteFragment: Fragment(), PlacemarkListener, AnkoLogger{
  private val UPDATE_PLACEMARK_RESULT_CODE=2
  lateinit var app:MainApp
  lateinit var presenter: FavouritePresenter
  lateinit var viewManager: RecyclerView.LayoutManager
  lateinit var viewAdapter: PlacemarkAdapter

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    val view=inflater.inflate(R.layout.fragment_favourite,container,false)
    app= MainApp.applicationObject!!
    presenter=FavouritePresenter(this)
    return view
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    viewAdapter= PlacemarkAdapter(presenter.placemarks, this)
    viewManager= LinearLayoutManager(context)
    favouritRecyclerView.apply{
      setHasFixedSize(true)
      layoutManager = viewManager
      adapter = viewAdapter
    }
    super.onViewCreated(view, savedInstanceState)
  }

  override fun onResume() {
    presenter.getPlacemark()
    super.onResume()
  }
  override fun onPlacemarkClicked(placemark: PlacemarkModel) {
    val intent= Intent(activity, PlacemarkView::class.java).putExtra("placemark",placemark)
    startActivityForResult(intent,UPDATE_PLACEMARK_RESULT_CODE)
  }

  fun showProgress(){
    favourite_progressBar.visibility=View.VISIBLE
  }

  fun hideProgress(){
    favourite_progressBar.visibility=View.GONE
  }
}