package org.wit.archaelo.activities.favourite

import android.opengl.Visibility
import android.view.View
import kotlinx.android.synthetic.main.fragment_favourite.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.wit.archaelo.models.PlacemarkModel

class FavouritePresenter(val view:FavouriteFragment):AnkoLogger{
  var app=view.app
  var placemarks= mutableListOf<PlacemarkModel>()

  fun getPlacemark(){
    view.showProgress()
    app.fireStore.fetchPlacemarks {
      view.hideProgress()
      val holder=app.fireStore.placemarks.filter {
        it.favourite
      }
      placemarks=holder.toMutableList()
      view.empty_text.visibility=if (placemarks.size==0)View.VISIBLE else View.GONE
      view.viewAdapter.updateData(placemarks)
    }
  }
}