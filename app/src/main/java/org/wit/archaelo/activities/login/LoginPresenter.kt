package org.wit.archaelo.activities.login

import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.startActivity
import org.wit.archaelo.R
import org.wit.archaelo.activities.main.MainView
import org.wit.archaelo.helpers.alert
import org.wit.archaelo.main.MainApp

class LoginPresenter(val view: LoginView):AnkoLogger{

  private var auth= FirebaseAuth.getInstance()
  var app=view.application as MainApp
  private var fireStore=app.fireStore

  init{
    view.showProgress()
    val currentUser = auth.currentUser
    if (currentUser!=null){
      fireStore.fetchPlacemarks {view.startActivity<MainView>()   }
    }
    view.hideProgresss()
  }

  fun doLogin(email: String, password: String) {
    if (email.isNullOrEmpty() || password.isNullOrEmpty()){
      view.alert("Login fail","Please fill in both email and password",R.color.errorColor)
    }
    else{
      view.showProgress()
      auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(view) { task ->
        if (task.isSuccessful) {
          alert(view,"Logged in successfully","You have logged into Archaelo", R.color.successColor)
          info("working")
          fireStore.fetchPlacemarks {
            info("working")
            view.startActivity<MainView>()
          }

        } else {
          alert(view,"Logged in fail",task.exception?.message, R.color.errorColor)
        }
        view.hideProgresss()
      }
    }
  }
}