package org.wit.archaelo.activities.login

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.tapadoo.alerter.Alerter
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.startActivityForResult
import org.wit.archaelo.R
import org.wit.archaelo.activities.main.MainView
import org.wit.archaelo.activities.register.RegisterView
import org.wit.archaelo.main.MainApp
import org.wit.archaelo.models.UserModel
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class LoginView : AppCompatActivity(),AnkoLogger {
  lateinit var app: MainApp
  lateinit var presenter: LoginPresenter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_login)
    progressBar.visibility= View.GONE
    app=application as MainApp
    presenter= LoginPresenter(this)

    var status=getSharedPreferences("archaelo", Context.MODE_PRIVATE).getString("status","")

    if(status=="log"){
      //val authenticatedUserEmail=getSharedPreferences("archaelo", Context.MODE_PRIVATE).getString("userEmail","")
      //val authenticatedUser=app.users.findUserWithEmail(authenticatedUserEmail)
      /*if (authenticatedUser is UserModel){
        app.user=authenticatedUser
        startActivity<MainView>()
      }*/
    }

    registerBtn.setOnClickListener{
      startActivityForResult<RegisterView>(1)
    }

    loginBtn.setOnClickListener{
      var email=loginEmailInput.text.toString()
      var password=loginPasswordInput.text.toString()
      presenter.doLogin(email,password)
    }
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    when(resultCode){
      1->{
        alert("Registered successfuly","You can now log in to your account", R.color.successColor)
        info  ("seems working")
      }
    }
  }

  //Typography
  override fun attachBaseContext(newBase: Context) {
    super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
  }

  fun showProgress(){
    progressBar.visibility=View.VISIBLE
  }

  fun hideProgresss(){
    progressBar.visibility=View.GONE
  }

  fun alert(title:String,message:String,color:Int){
    Alerter.create(this)
        .setTitle(title)
        .setText(message)
        .setBackgroundColorRes(color)
        .enableSwipeToDismiss().show()
  }
}
