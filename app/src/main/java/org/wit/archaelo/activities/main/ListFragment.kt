package org.wit.archaelo.activities.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.notification_template_lines_media.view.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.startActivityForResult
import org.wit.archaelo.R
import org.wit.archaelo.activities.PlacemarkAdapter
import org.wit.archaelo.activities.PlacemarkListener
import org.wit.archaelo.activities.placemark.PlacemarkView
import org.wit.archaelo.helpers.OnSwipeTouchListener
import org.wit.archaelo.helpers.alert
import org.wit.archaelo.main.MainApp
import org.wit.archaelo.models.PlacemarkModel

class ListFragment:Fragment(), PlacemarkListener,AnkoLogger {

  private val ADD_PLACEMARK_RESULT_CODE=1
  private val UPDATE_PLACEMARK_RESULT_CODE=2
  private val REMOVE_PLACEMARK_RESULT_CODE=3

  lateinit var app:MainApp
  lateinit var viewManager: RecyclerView.LayoutManager
  lateinit var viewAdapter: PlacemarkAdapter
  lateinit var presenter:ListPresenter

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    val view=inflater.inflate(R.layout.fragment_list,container,false)
    app= MainApp.applicationObject!!
    return view
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    parent.requestFocus()
    presenter= ListPresenter(this)
    viewAdapter= PlacemarkAdapter(presenter.placemarks, this)
    viewManager= LinearLayoutManager(context)
    placemarkRecyclerView.apply{
      setHasFixedSize(true)
      layoutManager = viewManager
      adapter = viewAdapter
    }

    floating_action_button.setOnClickListener {
      val intent=Intent(activity,PlacemarkView::class.java)
      startActivityForResult(intent,ADD_PLACEMARK_RESULT_CODE)
    }
    searchInput.setOnQueryTextListener(object:SearchView.OnQueryTextListener{
      override fun onQueryTextChange(newText: String?): Boolean {
        if (newText.isNullOrEmpty()){
          presenter.getPlacemark()
          viewAdapter.updateData(presenter.placemarks)
        }
        return true
      }

      override fun onQueryTextSubmit(query: String?): Boolean {
        info("Search query is $query")
        if (query!=null){
          presenter.getPlacemark(fun(){
            presenter.placemarks=presenter.placemarks.filter {
              it.name.contains(query,true)
              //|| it.description.contains(query,true)
            }
            viewAdapter.updateData(presenter.placemarks)
          })
        }
        return true
      }
    })

    super.onViewCreated(view, savedInstanceState)
  }

  override fun onResume() {
    presenter.getPlacemark()
    super.onResume()
  }
  override fun onPlacemarkClicked(placemark: PlacemarkModel) {
    val intent=Intent(activity,PlacemarkView::class.java).putExtra("placemark",placemark)
    startActivityForResult(intent,UPDATE_PLACEMARK_RESULT_CODE)
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    info("Ranning")
    when (resultCode){
      ADD_PLACEMARK_RESULT_CODE->{
        presenter.getPlacemark()
        alert(activity as MainView,"Placemark is added","A new placemark is created successfully", R.color.successColor)
      }
      UPDATE_PLACEMARK_RESULT_CODE->{
        presenter.getPlacemark()
        alert(activity as MainView,"Placemark is updated","An existing placemark is updated successfully", R.color.successColor)
      }
      REMOVE_PLACEMARK_RESULT_CODE->{
        presenter.getPlacemark()
        alert(activity as MainView,"Placemark is deleted","An existing placemark is deleted successfully", R.color.successColor)
      }
    }
  }
  fun showProgress(){
    progressBar.visibility=View.VISIBLE
  }

  fun hideProgress(){
    progressBar.visibility=View.GONE
  }
}