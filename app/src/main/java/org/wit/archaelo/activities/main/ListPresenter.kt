package org.wit.archaelo.activities.main

import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.wit.archaelo.activities.PlacemarkAdapter
import org.wit.archaelo.main.MainApp
import org.wit.archaelo.models.PlacemarkModel
import javax.security.auth.callback.Callback

class ListPresenter(val view: ListFragment): AnkoLogger {
  var app=view.app
  var placemarks= emptyList<PlacemarkModel>()
  init{
    view.showProgress()
    getPlacemark()
  }

  fun getPlacemark(callback: () -> Unit=fun(){}){
    app.fireStore.fetchPlacemarks {
      view.hideProgress()
      placemarks=app.fireStore.placemarks
      view.viewAdapter.updateData(placemarks)
      callback()
    }
  }

  fun onPlacemarsChanged(receivedPlacemarks:MutableList<PlacemarkModel>){
    //placemarks=receivedPlacemarks
  }
}