package org.wit.archaelo.activities.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.startActivityForResult
import org.wit.archaelo.R
import org.wit.archaelo.activities.placemark.PlacemarkView
import org.wit.archaelo.activities.PlacemarkAdapter
import org.wit.archaelo.activities.PlacemarkListener
import org.wit.archaelo.activities.card.CardFragment
import org.wit.archaelo.activities.favourite.FavouriteFragment
import org.wit.archaelo.activities.map.MapsView
import org.wit.archaelo.activities.profile.ProfileFragment
import org.wit.archaelo.helpers.OnSwipeTouchListener
import org.wit.archaelo.main.MainApp
import org.wit.archaelo.models.PlacemarkModel
import org.wit.archaelo.helpers.alert

class MainView : AppCompatActivity(),AnkoLogger {

  lateinit var app: MainApp
  //private lateinit var drawer: DrawerLayout


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    app=application as MainApp

    supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer,ListFragment()).commit()

    //toolbar.title=title
    //setSupportActionBar(toolbar)
    val actionbar: ActionBar? = supportActionBar
    actionbar?.apply {
      setDisplayHomeAsUpEnabled(true)
      setHomeAsUpIndicator(R.drawable.menu)
    }


    bottom_navigation.setOnNavigationItemSelectedListener { item->
      when (item.itemId){
        R.id.nav_list->{
          actionbar?.show()
          supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer,ListFragment()).commit()
        }
        R.id.nav_map->{
          actionbar?.hide()
          supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer,MapsView()).commit()
        }
        R.id.nav_fav->{
          actionbar?.hide()
          supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer,FavouriteFragment()).commit()
        }
        R.id.nav_profile->{
          actionbar?.hide()
          supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer,ProfileFragment()).commit()
        }
        R.id.nav_card->{
          supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer,CardFragment()).commit()
        }
      }
      true
    }
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      android.R.id.home -> {
        //drawer.openDrawer(GravityCompat.START)
        true
      }
      else -> super.onOptionsItemSelected(item)
    }
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.action_bar_menu,menu)
    supportActionBar!!.elevation=0.toFloat()
    return super.onCreateOptionsMenu(menu)
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode,resultCode,data)
  }
}

