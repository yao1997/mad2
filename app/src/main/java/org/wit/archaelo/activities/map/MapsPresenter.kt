package org.wit.archaelo.activities.map

import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.wit.archaelo.models.PlacemarkModel

class MapsPresenter(val view:MapsView): AnkoLogger {
  lateinit var placemarks:MutableList<PlacemarkModel>

  init{
    view.app.fireStore.fetchPlacemarks {
      placemarks=view.app.fireStore.placemarks
      view.addPlacemarksToMap(placemarks)
    }
  }
}