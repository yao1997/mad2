package org.wit.archaelo.activities.map

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.wit.archaelo.R
import org.wit.archaelo.main.MainApp
import org.wit.archaelo.models.PlacemarkModel
import java.util.jar.Manifest


class MapsView: Fragment(), OnMapReadyCallback, AnkoLogger, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener {
  private lateinit var mMap: GoogleMap
  private lateinit var fusedLocationClient: FusedLocationProviderClient
  private lateinit var presenter:MapsPresenter
  lateinit var app:MainApp

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    var view= inflater.inflate(R.layout.fragment_maps,container,false)
    app= MainApp.applicationObject!!
    presenter=MapsPresenter(this)

    //Setting map
    var map=childFragmentManager.findFragmentByTag("map_fragment") as SupportMapFragment?
    if (map==null){
      info("It is null")
    }
    map?.getMapAsync(this)

    //Getting location client
    if (this.context!=null)
      fusedLocationClient=LocationServices.getFusedLocationProviderClient(this.context as Context)

    return view
  }


  override fun onMapReady(googleMap: GoogleMap) {
    mMap = googleMap
    moveToCurrentLocation()
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
    when(requestCode){
      1->{
        if (grantResults[0]==PackageManager.PERMISSION_GRANTED){
          moveToCurrentLocation()
        }
      }
    }
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
  }

  override fun onMyLocationButtonClick(): Boolean { return false}

  override fun onMyLocationClick(location: Location) {}


  //Utility functions
  private fun moveToCurrentLocation(){
    if (ContextCompat.checkSelfPermission(activity!!.applicationContext,android.Manifest.permission.ACCESS_COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED ){
      mMap.isMyLocationEnabled=true
      fusedLocationClient.lastLocation.addOnSuccessListener {
        location : Location? ->
        val currentLatLng=if (location!=null)LatLng(location.latitude,location.longitude) else null
        if (currentLatLng!=null){
          mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,13.0f))
        }
      }
    }
    else{
      requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),1)
    }
  }

  fun addPlacemarksToMap(placemarks:List<PlacemarkModel>){
    placemarks.forEach{
      val location=LatLng(it.location.lat,it.location.lng)
      mMap.addMarker(MarkerOptions().position(location).title(it.name))
    }
  }
}