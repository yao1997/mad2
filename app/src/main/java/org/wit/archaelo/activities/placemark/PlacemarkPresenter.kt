package org.wit.archaelo.activities.placemark

import kotlinx.android.synthetic.main.activity_placemark.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.startActivityForResult
import org.wit.archaelo.R
import org.wit.archaelo.activities.MapsActivity
import org.wit.archaelo.helpers.alert
import org.wit.archaelo.main.MainApp
import org.wit.archaelo.models.PlacemarkModel

class PlacemarkPresenter(val view:PlacemarkView):AnkoLogger{

  private val ADD_PLACEMARK_RESULT_CODE=1
  private val UPDATE_PLACEMARK_RESULT_CODE=2
  private val REMOVE_PLACEMARK_RESULT_CODE=3
  val SELECT_IMAGE_REQUEST=1
  val SELECT_LOCATION_REQUEST=2
  val SELECT_DP_REQUEST=3
  val SELECT_CAMERA_REQUEST=4


  var placemark=PlacemarkModel()
  var edit=false
  var app=view.application as MainApp


  fun doAddOrEdit(name:String,description:String){
    placemark.name=name
    placemark.description=description
    if (edit){
      view.showLoading()
      app.fireStore.update(placemark,fun(){
        view.hideLoading()
        view.setResult(UPDATE_PLACEMARK_RESULT_CODE)
        view.finish()
      })
    }
    else{
      view.showLoading()
      app.fireStore.create(placemark,fun(){
        view.hideLoading()
        view.setResult(ADD_PLACEMARK_RESULT_CODE)
        view.finish()
      })
    }
  }

  fun changeFavourite(value:Boolean){
    placemark.favourite=value
    app.fireStore.update(placemark,fun(){
      view.changeFavouriteIcon(value)
    })
  }

  fun removePlacemark(){
    app.fireStore.delete(placemark)
    view.setResult(REMOVE_PLACEMARK_RESULT_CODE)
    view.finish()
  }

  fun doSetLocation(){
    view.startActivityForResult<MapsActivity>(SELECT_LOCATION_REQUEST, "location" to placemark.location)
  }

  fun validateInput(name:String,description: String,dp:String,images:MutableList<String>,visited:Boolean,visitedDate:String):Boolean{
    var result=false
    info("Placemark is ${placemark.dp}")
    if (name.isBlank()){
      alert(view,"Please fill in all fields","Placemark name is empty", R.color.errorColor)
    }
    else if (description.isBlank()){
      alert(view,"Please fill in all fields","Placemark additional notes is empty", R.color.errorColor)
    }
    else if (dp.isBlank()){
      alert(view,"Please upload image","Your lecturer requested a profile image as proof", R.color.errorColor)
    }
    /*else if (images.isEmpty()){
      alert(view,"Please upload image","Your lecturer requested at least 1 image as proof", R.color.errorColor)
    }*/
    else if (visited && visitedDate==view.resources.getText(R.string.set_placemark_date_hint)){
      alert(view,"Please select visited date","Select a visited date or mark as unvisited.", R.color.errorColor)
    }
    else{
      result=true
    }
    return result
  }



}
