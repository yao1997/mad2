package org.wit.archaelo.activities.placemark

import android.app.ActionBar
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import org.wit.archaelo.R
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import android.content.Intent
import android.view.*
import android.widget.*
import androidx.databinding.DataBindingUtil
import com.esafirm.imagepicker.features.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_placemark.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.wit.archaelo.databinding.ActivityPlacemarkBinding
import org.wit.archaelo.main.MainApp
import org.wit.archaelo.models.LocationModel
import org.wit.archaelo.models.PlacemarkModel
import java.io.File
import java.net.URI
import java.util.*


class PlacemarkView : AppCompatActivity(),AnkoLogger, DatePickerDialog.OnDateSetListener,dialogListener {

  private val SELECT_IMAGE_REQUEST=1
  private val SELECT_LOCATION_REQUEST=2
  var placemark=PlacemarkModel()
  lateinit var app: MainApp
  lateinit var presenter: PlacemarkPresenter
  lateinit var binding: ActivityPlacemarkBinding
  lateinit var actionBarMenu:Menu
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_placemark)

    presenter= PlacemarkPresenter(this)
    binding=DataBindingUtil.setContentView<ActivityPlacemarkBinding>(this,R.layout.activity_placemark)
    binding.placemark=placemark
    binding.presenter=presenter
    binding.view=this

    setSupportActionBar(addPlacemark_toolbar)
    supportActionBar?.apply {
      setDisplayHomeAsUpEnabled(true)
      setHomeAsUpIndicator(R.drawable.toolbar_back)
      setDisplayShowTitleEnabled(false)
    }

    /********************View Event Listener*******************/
    setImageBtn.setOnClickListener {
      /*ImagePicker.create(this).multi()
          .showCamera(true).limit(5)
          .start(SELECT_IMAGE_REQUEST)*/
      //ImagePicker.cameraOnly().start(this)x
      val alert= AlertDialog.Builder(this).create()
      alert.setTitle("Choose Action")
      alert.setMessage("How would you like to upload images")
      alert.setButton(Dialog.BUTTON_POSITIVE,"Camera"){listener,num->
        presenter.placemark.images= mutableListOf()
        imgContainer.removeAllViews()
        ImagePicker.cameraOnly().start(this,presenter.SELECT_CAMERA_REQUEST)
      }
      alert.setButton(Dialog.BUTTON_NEUTRAL,"Gallery"){listener,num->
        presenter.placemark.images= mutableListOf()
        imgContainer.removeAllViews()
        ImagePicker.create(this).multi()
            .showCamera(true).limit(5)
            .start(SELECT_IMAGE_REQUEST)
      }
      alert.show()
    }

    setLocationBtn.setOnClickListener{
      presenter.doSetLocation()
    }

    //dateInput.isEnabled=visitedInput.isChecked
    dateInput.setOnClickListener{
      val dialog = DatePickerDialog(this, this,Calendar.getInstance().get(Calendar.YEAR) ,
          Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
      dialog.show()
    }

    if (this.intent.hasExtra("placemark")){
      addPlacemark_btn.text="Update"
      initiateEditPlacemark(this.intent.getParcelableExtra("placemark"))
    }

    addPlacemark_btn.setOnClickListener{
      if (presenter.validateInput(
          nameInput.text.toString(),
          descriptionInput.text.toString(),
          presenter.placemark.dp,
          presenter.placemark.images,
          presenter.placemark.visited,
          dateInput.text.toString()
      )){
        presenter.doAddOrEdit(nameInput.text.toString(),descriptionInput.text.toString())
      }
    }

    ratingButton.setOnClickListener{
      var dialog=RatingDialogFragment()
      dialog.show(supportFragmentManager,"Rating Fragment")
    }

    fab.setOnClickListener{
      ImagePicker.create(this).single()
          .showCamera(true) // Activity or Fragment
          .start(presenter.SELECT_DP_REQUEST)
    }
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      android.R.id.home->{
        onBackPressed()
        true
      }
      R.id.action_bar_remove->{
        presenter.removePlacemark()
        true
      }
      R.id.action_bar_like->{
        presenter.changeFavourite(!presenter.placemark.favourite)
        true
      }
      R.id.action_bar_share->{
        var intent=Intent(Intent.ACTION_SEND)
        intent.setType("text/plain")
        intent.putExtra(Intent.EXTRA_SUBJECT, "A Cool Placemark");
        intent.putExtra(Intent.EXTRA_TEXT, "Hi I'm sharing this amazing placemark to you via app Boolean.\nName:${presenter.placemark.name}\n" +
            "Description: ${presenter.placemark.description}");
        startActivity(Intent.createChooser(intent, "Send Email"))
        true
      }
      else->super.onOptionsItemSelected(item)
    }
  }
  //Typography
  override fun attachBaseContext(newBase: Context) {
    super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    when(requestCode){
      presenter.SELECT_DP_REQUEST->{
        val image = ImagePicker.getFirstImageOrNull(data)
        if (image!=null){
          presenter.placemark.dp=image.path
          Picasso.get().load(File(image.path)).into(dp_holder)
          //dp.setImageBitmap(BitmapFactory.decodeFile(image.getPath()))
        }
      }
      presenter.SELECT_IMAGE_REQUEST->{
        val images=ImagePicker.getImages(data)
        if (images!=null) {
          presenter.placemark.images.clear()
          for (each in images) {
            presenter.placemark.images.add(each.path)
            addImage(File(each.path))
          }
        }
      }
      presenter.SELECT_CAMERA_REQUEST->{
        val image=ImagePicker.getFirstImageOrNull(data)
        if (image!=null){
          presenter.placemark.images.add(image.path)
          addImage(File(image.path))
        }
      }
      SELECT_LOCATION_REQUEST->{
        presenter.placemark.location=data!!.extras!!.getParcelable<LocationModel>("location")
      }
    }
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.add_placemark_menu,menu)
    actionBarMenu=menu!!
    changeFavouriteIcon(presenter.placemark.favourite)
    if(!intent.hasExtra("placemark")) {
      val item = menu.findItem(R.id.action_bar_remove)
      item?.isVisible=false
    }
    //actionBar?.setIcon(R.color.transparent)
    return super.onCreateOptionsMenu(menu)
  }

  override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
    val dateChosen = "    "+dayOfMonth.toString() + "/" + (month + 1) + "/" + year
    presenter.placemark.visitedDay=dayOfMonth.toString()
    presenter.placemark.visitedMonth=month.toString()
    presenter.placemark.visitedYear=year.toString()
    dateInput.text=dateChosen
  }

  override fun onReturnValue(rating: Float) {
    info("Rating is cute $rating")
    presenter.placemark.rating=rating
  }

  fun initiateEditPlacemark(passedPlacemark: PlacemarkModel){
    presenter.edit=true
    info("Placemark working is ${presenter.edit}")
    presenter.placemark = passedPlacemark
    binding.invalidateAll()
    binding.placemark=presenter.placemark
    visitedInput.isChecked = presenter.placemark.visited
    if (presenter.placemark.visited && presenter.placemark.visitedDay != "") {
     val dateString = "    " + presenter.placemark.visitedDay.toString() + "/" + (presenter.placemark.visitedMonth.toInt() + 1) + "/" + presenter.placemark.visitedYear
     dateInput.text = dateString
    }

    if (presenter.placemark.dp!=""){
      Picasso.get().load(presenter.placemark.dp).placeholder(R.drawable.placeholder).into(dp_holder)
    }

    if (presenter.placemark.images.size>0){
      presenter.placemark.images.forEach{
        addImage(it)
      }
    }
  }

  fun onVisitedInputChanged(checked:Boolean){
    presenter.placemark.visited=checked
    dateInput.isEnabled = checked
    if(!checked){
      dateInput.text=resources.getText(R.string.set_placemark_date_hint)
      dateInput.setBackgroundColor(resources.getColor(R.color.uneditable_background))
    }
    else {
      dateInput.setBackgroundColor(resources.getColor(R.color.editable_background))
    }
  }

  fun onDescriptionChanged(text:CharSequence,s:Int,e:Int,c:Int){
    addPlacemark_notes.text=text
  }

  fun onNameChanged(text:CharSequence,s:Int,e:Int,c:Int){
    addPlacemark_title.text=text
  }

  fun addImage(img:Any){
    info("Image is $img")
    val placeholder=ImageView(this)
    val lp=RelativeLayout.LayoutParams(400,ViewGroup.LayoutParams.MATCH_PARENT)
    lp.setMargins(10,0,0,0)
    placeholder.layoutParams=lp
    if (img is File){
      Picasso.get().load(img).centerCrop().fit().into(placeholder)
    }
    else if (img is String){
      Picasso.get().load(img).placeholder(R.drawable.placeholder).centerCrop().fit().into(placeholder)
    }
    imgContainer.addView(placeholder)
  }

  fun showLoading(){
    placemark_loading.visibility=View.VISIBLE
  }

  fun hideLoading(){
    placemark_loading.visibility=View.GONE
  }

  fun changeFavouriteIcon(value:Boolean){
    if (value){
      actionBarMenu.getItem(1).setIcon(R.drawable.toolbar_like_added)
    }
    else{
      actionBarMenu.getItem(1).setIcon(R.drawable.toolbar_like)
    }
  }
}

interface dialogListener{
  fun onReturnValue(rating:Float)
}