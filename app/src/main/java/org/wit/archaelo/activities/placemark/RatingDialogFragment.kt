package org.wit.archaelo.activities.placemark

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.method.DialerKeyListener
import android.view.View
import android.widget.RatingBar
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.fragment_rating.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.wit.archaelo.R

class RatingDialogFragment:DialogFragment(),AnkoLogger{
  override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
    return activity?.let {
      val builder = AlertDialog.Builder(it)
      val inflator=this.activity!!.layoutInflater
      info("Working")
      val view=inflator.inflate(R.layout.fragment_rating,null)
      builder.setView(view)
          // Add action buttons
          .setPositiveButton("Confirm"){_,_->
            val result=view.findViewById<RatingBar>(R.id.ratingBar)
            val activity=activity as dialogListener
            activity.onReturnValue(result.rating)
            info("Rating is great ${result.rating}")
          }
          .setNegativeButton("Cancel"){_,_->
            info("Working is super great")
          }
      builder.create()
    } ?: throw IllegalStateException("Activity cannot be null")
  }
}

/*override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
  return activity?.let {
    val builder = AlertDialog.Builder(it)

    // Inflate and set the layout for the dialog
    // Pass null as the parent view because its going in the dialog layout
    builder.setView(layoutInflater.inflate(R.layout.fragment_rating,null,false))
        // Add action buttons
        .setPositiveButton("Confirm"){_,_->
          info("Working is great")
        }
        .setNegativeButton("Cancel"){_,_->
          info("Working is super great")
        }
    builder.create()
  } ?: throw IllegalStateException("Activity cannot be null")
}*/