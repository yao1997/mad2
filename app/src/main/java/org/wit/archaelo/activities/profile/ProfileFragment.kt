package org.wit.archaelo.activities.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_profile.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.wit.archaelo.R
import org.wit.archaelo.databinding.FragmentProfileBinding
import org.wit.archaelo.main.MainApp

class ProfileFragment:Fragment(),AnkoLogger{
  lateinit var app:MainApp
  lateinit var presenter: ProfilePresenter
  lateinit var binding: FragmentProfileBinding

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    //val view=inflater.inflate(R.layout.fragment_profile,container,false)
    binding= DataBindingUtil.inflate(inflater,R.layout.fragment_profile,container,false)
    app=MainApp.applicationObject!!
    presenter=ProfilePresenter(this)

    return binding.root
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    presenter.initProfile()
    binding.user=presenter.user
    binding.presenter=presenter
    super.onViewCreated(view, savedInstanceState)
  }

}