package org.wit.archaelo.activities.profile

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import com.esafirm.imagepicker.features.ImagePicker
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_placemark.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.fragment_profile.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.wit.archaelo.R
import org.wit.archaelo.activities.login.LoginView
import org.wit.archaelo.helpers.alert
import org.wit.archaelo.models.UserModel

class ProfilePresenter(val view:ProfileFragment):AnkoLogger{
  var app=view.app
  var user=UserModel()
  private var auth= FirebaseAuth.getInstance()
  private var db = FirebaseDatabase.getInstance().reference

  init{
    user.id=auth.uid!!
  }

  fun initProfile(){
    val valueEventListener = object : ValueEventListener {
      override fun onCancelled(error: DatabaseError) {
      }
      override fun onDataChange(dataSnapshot: DataSnapshot) {
        info("User profile")
        if (dataSnapshot.getValue<UserModel>(UserModel::class.java)!=null){
          user=dataSnapshot.getValue<UserModel>(UserModel::class.java)!!
          view.binding.user=user
          view.binding.user!!.email=auth!!.currentUser!!.email!!
          if (user.name.isNullOrEmpty()){
            view.binding.user!!.name=auth!!.currentUser!!.displayName!!
          }
          info("User profile ${view.binding.user}")
        }
        else{
          info("User profile is running")
          view.binding.user!!.email=auth!!.currentUser!!.email!!
          view.binding.user!!.name=auth!!.currentUser!!.displayName!!
          info("User profile is running ${view.binding.user}")
        }
      }
    }
    db.child("users").child(FirebaseAuth.getInstance().currentUser!!.uid).child("profile").addListenerForSingleValueEvent(valueEventListener)
  }

  fun updateProfile(){
    info("Profile is ${user.name}")
    /*user.name=view.profile_nameInput.text.toString()
    user.age=view.profile_ageInput.text.toString()
    user.studentId=view.profile_studentIdInput.text.toString()*/
    db.child("users").child(FirebaseAuth.getInstance().currentUser!!.uid).child("profile").setValue(user){_,_->
      alert(view.activity!!,"Success","Profile is updated successfully", R.color.successColor)
    }
  }

  fun logout(){
    val alert= AlertDialog.Builder(view.context).create()
    alert.setTitle("Choose Action")
    alert.setMessage("Would you like to logout")
    alert.setButton(Dialog.BUTTON_POSITIVE,"Log out"){ listener, num->
      auth.signOut()
      val intent = Intent(view.context, LoginView::class.java)
      view.startActivity(intent)
    }
    alert.setButton(Dialog.BUTTON_NEGATIVE,"Cancel"){ listener, num->
      alert.hide()
    }
    alert.show()
  }
}