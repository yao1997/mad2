package org.wit.archaelo.activities.register

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.FirebaseDatabase
import org.wit.archaelo.R
import org.wit.archaelo.activities.login.LoginView
import org.wit.archaelo.helpers.alert
import org.wit.archaelo.main.MainApp
import org.wit.archaelo.models.UserModel

class RegisterPresenter(val view: RegisterView){
  private var auth= FirebaseAuth.getInstance()
  var app=view.application as MainApp
  private var db = FirebaseDatabase.getInstance().reference

  init{}

  fun doRegister(email:String,password:String,name:String){
    auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(view){ task->
      if (task.isSuccessful){
        alert(view,"User is created successfully","You can now log into Archaelo", R.color.successColor)
        UserProfileChangeRequest.Builder().setDisplayName(name).build()

        var user=UserModel(email=email,name=name)
        db.child("users").child(FirebaseAuth.getInstance().currentUser!!.uid).child("profile").setValue(user){err,ref->
          view.setResult(1)
          view.finish()
        }
      }
      else{
        alert(view,"Registration Fail",task.exception?.message, R.color.errorColor)
      }
    }
  }
}