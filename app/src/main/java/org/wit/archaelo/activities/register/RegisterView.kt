package org.wit.archaelo.activities.register

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.AnkoLogger
import org.wit.archaelo.R
import org.wit.archaelo.models.UserModel
import org.wit.archaelo.main.MainApp
import org.wit.archaelo.helpers.alert
import android.text.Spanned
import org.wit.archaelo.helpers.isEmailValid
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


class RegisterView : AppCompatActivity(),AnkoLogger {
  lateinit var app:MainApp
  lateinit var presenter: RegisterPresenter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    presenter= RegisterPresenter(this)
    setContentView(R.layout.activity_register)
    app=application as MainApp
    val htmlString= "Already registered? <font color='blue'>Click here</font> to log in"
    loginText.setText(fromHtml(htmlString), TextView.BufferType.SPANNABLE)

    loginText.setOnClickListener{
      finish()
    }

    confirmRegisterBtn.setOnClickListener{
      var email=emailInput.text.toString()
      var password=passwordInput.text.toString()
      var confirmPassword=confirmPasswordInput.text.toString()
      var name=nameInput.text.toString()
      if (email.isEmpty()){
        alert(this,"Please fill in all fields","Email field is empty", R.color.errorColor)
      }
      else if (password.isEmpty()){
        alert(this,"Please fill in all fields","Password field is empty", R.color.errorColor)
      }
      else if (confirmPassword.isEmpty()){
        alert(this,"Please fill in all fields","Confirm Password field is empty", R.color.errorColor)
      }
      else if (name.isEmpty()){
        alert(this,"Please fill in all fields","Name field is empty", R.color.errorColor)
      }
      else if (password!=confirmPassword){
        alert(this,"Please check password","Password is different with confirm password", R.color.errorColor)
      }
      else if (!isEmailValid(email)){
        alert(this,"Wrong email format","Email is an invalid email", R.color.errorColor)
      }
      else{
        presenter.doRegister(email,password,name)
      }
    }
  }

  //Typography
  override fun attachBaseContext(newBase: Context) {
    super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
  }

  fun fromHtml(html: String): Spanned {
    return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
  }
}
