package org.wit.archaelo.helpers

import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.annotation.GlideModule;

@GlideModule
class MyAppGlideModule:AppGlideModule()