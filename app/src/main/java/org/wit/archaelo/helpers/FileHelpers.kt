package org.wit.placemark.helpers

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonParser
import java.io.*
import org.wit.archaelo.main.MainApp


fun write(context: Context, fileName: String, data: String) {
  try {
    val outputStreamWriter = OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_PRIVATE))
    outputStreamWriter.write(data)
    outputStreamWriter.close()
  } catch (e: Exception) {
    Log.e("Error: ", "Cannot read file: " + e.toString());
  }
}
/*fun writeUnify(context: Context, fileName: String){
  Log.i("json","unify write")
  val users=MainApp.applicationObject!!.users.users
  val placemarks=MainApp.applicationObject!!.placemarks.placemarks
  var output= mapOf<String,List<Any>>("placemarks" to placemarks,"users" to users)
  var json=Gson().toJson(output)
  write(context,fileName,json.toString())
}*/

fun readUnify(context: Context, fileName: String,type:String):String?{
  Log.i("json","unify read")
  val str=read(context,fileName)
  Log.i("json",str)
  if (str!=""){
    val jsonObject=JsonParser().parse(str).asJsonObject
    val result=jsonObject.get(type).toString()
    Log.i("json",JsonParser().parse(str).asJsonObject.get("users").toString())
    return result
  }
  else{
    return ""
  }
}


fun read(context: Context, fileName: String): String {
  var str = ""
  try {
    val inputStream = context.openFileInput(fileName)
    if (inputStream != null) {
      val inputStreamReader = InputStreamReader(inputStream)
      val bufferedReader = BufferedReader(inputStreamReader)
      val partialStr = StringBuilder()
      var done = false
      while (!done) {
        var line = bufferedReader.readLine()
        done = (line == null);
        if (line != null) partialStr.append(line);
      }
      inputStream.close()
      str = partialStr.toString()
    }
  } catch (e: FileNotFoundException) {
    Log.e("Error: ", "file not found: " + e.toString());
  } catch (e: IOException) {
    Log.e("Error: ", "cannot read file: " + e.toString());
  }
  //Log.i("json",str)
  return str
}

fun exists(context: Context, filename: String): Boolean {
  val file = context.getFileStreamPath(filename)
  return file.exists()
}