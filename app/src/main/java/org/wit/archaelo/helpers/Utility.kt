package org.wit.archaelo.helpers

import android.app.Activity
import android.content.Context
import com.tapadoo.alerter.Alerter


fun alert(activity: Activity,title:String,message:String?,color:Int){
  if(message!=null)
  Alerter.create(activity)
      .setTitle(title)
      .setText(message)
      .setBackgroundColorRes(color)
      .enableSwipeToDismiss().show()
}

fun isEmailValid(email:String):Boolean{
  val emailPattern = Regex("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")
  if (email.matches(emailPattern)){
    return true
  }
  return false
}