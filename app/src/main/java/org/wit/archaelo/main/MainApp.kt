package org.wit.archaelo.main

import android.app.Application
import org.wit.archaelo.R
import org.wit.archaelo.models.UserModel
import org.wit.placemark.models.firebase.PlacemarkFireStore
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import android.util.Log


class MainApp : Application() {
  lateinit var fireStore: PlacemarkFireStore
  lateinit var user: UserModel

  companion object {
    var applicationObject:MainApp?=null
  }
  override fun onCreate() {
    super.onCreate()
    applicationObject= this

    CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
        .setDefaultFontPath("fonts/Roboto-Thin.ttf")
        .setFontAttrId(R.attr.fontPath)
        .build()
    )
    fireStore=PlacemarkFireStore(this)
    Log.i("Fetching placemarks","Main app running")
  }
}
