package org.wit.archaelo.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LocationModel(var lat: Double=0.00,
                    var lng: Double=0.00,
                    var zoom: Float=0.toFloat()) : Parcelable