package org.wit.archaelo.models

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlacemarkModel(var id:String="",
                          var name:String="",
                          var description:String="",
                          var visited:Boolean=false,
                          var visitedYear:String="",
                          var visitedMonth:String="",
                          var visitedDay:String="",
                          var rating:Float=0.toFloat(),
                          var favourite:Boolean=false,
                          var location:LocationModel=LocationModel(52.2459,-7.1388,17.0f),
                          var dp:String="",
                          var images:MutableList<String> = mutableListOf()
                          ):Parcelable
