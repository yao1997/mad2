package org.wit.archaelo.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserModel(var id:String="",
                     var email:String="",
                     var name:String="",
                     var age:String="",
                     var studentId:String=""):Parcelable