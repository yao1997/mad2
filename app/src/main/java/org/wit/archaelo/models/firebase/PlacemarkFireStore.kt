package org.wit.placemark.models.firebase

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.annotation.UiThread
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import org.wit.archaelo.helpers.readAndCompress
import org.wit.archaelo.models.PlacemarkModel
import java.io.ByteArrayOutputStream
import java.io.File
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import org.jetbrains.anko.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import java.net.URI

class PlacemarkFireStore(val context: Context) :  AnkoLogger {

  var placemarks = ArrayList<PlacemarkModel>()
  lateinit var userId: String
  private var db = FirebaseDatabase.getInstance().reference
  private var st: StorageReference? = null

  fun findById(id: String): PlacemarkModel? {
    val foundPlacemark: PlacemarkModel? = placemarks.find { p -> p.id == id }
    return foundPlacemark
  }

  fun create(placemark: PlacemarkModel,callback: () -> Unit=fun(){}) {
    val key = db.child("users").child(userId).child("placemarks").push().key
    placemark.id = key!!
    val tasks= arrayListOf<Task<Uri>>()

    //Adding tasks into task list
    val dpTask=uploadImage(placemark.dp)
    if (dpTask!=null)tasks.add(dpTask)
    if (placemark.images.size>0){
      placemark.images.forEach{
        val task=uploadImage(it)
        if (task!=null)tasks.add(task)
      }
    }
    //Execute callback when tasks are completed
    Tasks.whenAllSuccess<Uri>(tasks).addOnCompleteListener { tasks->
      val results=tasks.result
      val resultsString=results!!.map{
        it.toString()
      }
      placemark.dp=resultsString[0]
      placemark.images=resultsString.subList(1,resultsString.size).toMutableList()
      db.child("users").child(userId).child("placemarks").child(key).setValue(placemark){err,ref->
        callback()
      }
    }
  }

  fun update(placemark: PlacemarkModel,callback: () -> Unit=fun(){}) {
    val foundPlacemark: PlacemarkModel? = placemarks.find { p -> p.id == placemark.id }
    val tasks= arrayListOf<Task<Uri>>()
    if (foundPlacemark != null) {
      if (placemark.dp.substring(0,5)!="https"){
        tasks.add(uploadImage(placemark.dp)!!)
      }
      if (placemark.images.size>0 && placemark.images[0].substring(0,5)!="https"){
        placemark.images.forEach{
          info("Runned once")
          tasks.add(uploadImage(it)!!)
        }
      }
      if (tasks.size>0){
        Tasks.whenAllSuccess<Uri>(tasks).addOnCompleteListener { tasks->
          val results=tasks.result
          val resultsString=results!!.map{
            it.toString()
          }
          if (placemark.dp.substring(0,5)!="https"){
            placemark.dp=resultsString[0]
            placemark.images=resultsString.subList(1,resultsString.size).toMutableList()
          }
          else{
            placemark.images=resultsString.toMutableList()
          }
          db.child("users").child(userId).child("placemarks").child(foundPlacemark.id).setValue(placemark){err,ref->
            callback()
          }
        }
      }
      else{
        db.child("users").child(userId).child("placemarks").child(foundPlacemark.id).setValue(placemark){err,ref->
          callback()
        }
      }
    }
  }

  fun uploadImage(image:String):Task<Uri>?
 {
    st = FirebaseStorage.getInstance().reference
    val imageRef = st?.child(userId + '/' + File(image).name)
    val data=readAndCompress(image)
    val task=if (data!=null)imageRef?.putBytes(data) else null
    return task?.continueWithTask {_->
      info("Coroutine is ${imageRef?.downloadUrl}")
      (imageRef?.downloadUrl)
    }
  }

  fun delete(placemark: PlacemarkModel) {
    db.child("users").child(userId).child("placemarks").child(placemark.id).removeValue()
    placemarks.remove(placemark)
  }


  fun clear() {
    placemarks.clear()
  }

  fun fetchPlacemarks(placemarksReady: () -> Unit) {
    val valueEventListener = object : ValueEventListener {
      override fun onCancelled(error: DatabaseError) {
      }
      override fun onDataChange(dataSnapshot: DataSnapshot) {
        info("Data changed ")
        placemarks=ArrayList<PlacemarkModel>()
        dataSnapshot.children.mapNotNullTo(placemarks) {
          it.getValue<PlacemarkModel>(PlacemarkModel::class.java)
        }
        info("Data is  ${placemarks}")
        placemarksReady()
      }
    }
    userId = FirebaseAuth.getInstance().currentUser!!.uid
    db.child("users").child(userId).child("placemarks").addListenerForSingleValueEvent(valueEventListener)
  }
}

/*, fun() {
              db.child("users").child(userId).child("placemarks").child(placemark.id).setValue(placemark) { _, _ ->
                info("Working fuck")
                callback
              }
            }*/
